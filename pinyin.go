// Package pinyin provides functions to convert text between Pinyin diacritic and tone markings, e.g. wo3 <-> wǒ.
package pinyin

import (
	"regexp"
	"strconv"
	"strings"
)

// Pinyin neutral tone integer
const neutralTone = 5

// toneChar represents a combo like {Tone: 1, Char: a}
type toneChar struct {
	Tone int
	Char rune
}

var (
	// All tones from 1-5 are valid.
	pinyinNumeralWordRe = regexp.MustCompile("(?i)[a-z]+[1-5]")
	// Any string of letters that contains one of the following diacritics is valid
	pinyinDiacriticWordRe = regexp.MustCompile("(?i)[a-z]*[āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜ][a-z]*")
	// ü is a different sound from u and its own Pinyin vowel, typed as v
	vowels = map[rune]bool{
		'a': true,
		'e': true,
		'i': true,
		'o': true,
		'u': true,
		'v': true,
	}
	toneCharToDiacritic = map[toneChar]rune{
		// a
		toneChar{Tone: 1, Char: 'a'}: 'ā',
		toneChar{Tone: 2, Char: 'a'}: 'á',
		toneChar{Tone: 3, Char: 'a'}: 'ǎ',
		toneChar{Tone: 4, Char: 'a'}: 'à',
		// e
		toneChar{Tone: 1, Char: 'e'}: 'ē',
		toneChar{Tone: 2, Char: 'e'}: 'é',
		toneChar{Tone: 3, Char: 'e'}: 'ě',
		toneChar{Tone: 4, Char: 'e'}: 'è',
		// i
		toneChar{Tone: 1, Char: 'i'}: 'ī',
		toneChar{Tone: 2, Char: 'i'}: 'í',
		toneChar{Tone: 3, Char: 'i'}: 'ǐ',
		toneChar{Tone: 4, Char: 'i'}: 'ì',
		// o
		toneChar{Tone: 1, Char: 'o'}: 'ō',
		toneChar{Tone: 2, Char: 'o'}: 'ó',
		toneChar{Tone: 3, Char: 'o'}: 'ǒ',
		toneChar{Tone: 4, Char: 'o'}: 'ò',
		// u
		toneChar{Tone: 1, Char: 'u'}: 'ū',
		toneChar{Tone: 2, Char: 'u'}: 'ú',
		toneChar{Tone: 3, Char: 'u'}: 'ǔ',
		toneChar{Tone: 4, Char: 'u'}: 'ù',
		// v
		toneChar{Tone: 1, Char: 'v'}: 'ǖ',
		toneChar{Tone: 2, Char: 'v'}: 'ǘ',
		toneChar{Tone: 3, Char: 'v'}: 'ǚ',
		toneChar{Tone: 4, Char: 'v'}: 'ǜ',
	}
	diacriticToToneChar = map[rune]toneChar{
		// a
		'ā': {Tone: 1, Char: 'a'},
		'á': {Tone: 2, Char: 'a'},
		'ǎ': {Tone: 3, Char: 'a'},
		'à': {Tone: 4, Char: 'a'},
		// e
		'ē': {Tone: 1, Char: 'e'},
		'é': {Tone: 2, Char: 'e'},
		'ě': {Tone: 3, Char: 'e'},
		'è': {Tone: 4, Char: 'e'},
		// i
		'ī': {Tone: 1, Char: 'i'},
		'í': {Tone: 2, Char: 'i'},
		'ǐ': {Tone: 3, Char: 'i'},
		'ì': {Tone: 4, Char: 'i'},
		// o
		'ō': {Tone: 1, Char: 'o'},
		'ó': {Tone: 2, Char: 'o'},
		'ǒ': {Tone: 3, Char: 'o'},
		'ò': {Tone: 4, Char: 'o'},
		// u
		'ū': {Tone: 1, Char: 'u'},
		'ú': {Tone: 2, Char: 'u'},
		'ǔ': {Tone: 3, Char: 'u'},
		'ù': {Tone: 4, Char: 'u'},
		// v
		'ǖ': {Tone: 1, Char: 'v'},
		'ǘ': {Tone: 2, Char: 'v'},
		'ǚ': {Tone: 3, Char: 'v'},
		'ǜ': {Tone: 4, Char: 'v'},
	}
)

// If we know the tone and which vowel to replace,
// replace it in the given string, and return the
// []byte representation of the replacement for regex
// replacement convenience
func markToneForVowel(in string, tone int, vowel rune) []byte {
	tC := toneChar{Tone: tone, Char: vowel}
	replacement := toneCharToDiacritic[tC]
	result := strings.Replace(in, string(vowel), string(replacement), -1)
	return []byte(result)
}

// toDiacritic converts a single word/tone combo to the diacritic version.
// Ex. wo3 -> wǒ
// See rules of Pinyin: http://www.pinyin.info/rules/where.html
func toDiacritic(inBytes []byte) []byte {
	in := string(inBytes)
	// The tone is the last character in the string
	tone64, _ := strconv.ParseInt(in[len(in)-1:], 0, 0)
	tone := int(tone64)
	in = in[:len(in)-1]
	var (
		numVowels int
		lastVowel rune
	)
	for _, r := range in {
		if _, isVowel := vowels[r]; isVowel {
			lastVowel = r
			numVowels += 1
		}
	}
	// Tone 5, the "netural tone," does not have a tone mark.
	if tone == neutralTone {
		return []byte(in)
	}
	// Single vowels always take the tone mark.
	if numVowels == 1 {
		return markToneForVowel(in, tone, lastVowel)
	}
	// a or e always takes the tone mark.
	simpleReplacements := []rune{'a', 'e'}
	for _, r := range simpleReplacements {
		if strings.Contains(in, string(r)) {
			return markToneForVowel(in, tone, r)
		}
	}
	// If ou is present, o takes the tone mark.
	if strings.Contains(in, "ou") {
		return markToneForVowel(in, tone, 'o')
	}
	// Otherwise, the second vowel takes the tone mark.
	return markToneForVowel(in, tone, lastVowel)
}

// ConvertToDiacritics converts any length Pinyin text marked with numerals,
// to Pinyin marked by diacritics. Tone 5 is an absence of diacritics.
// Ex. ConvertToDiacritics("wo3 hen3 hao3 a5") // wǒ hěn hǎo a
func ConvertToDiacritics(text string) string {
	replacedBytes := pinyinNumeralWordRe.ReplaceAllFunc([]byte(text), toDiacritic)
	return string(replacedBytes)
}

// toNumeral converts a diacritic-marked word to one without marks and with the tone number.
// Ex. wǒ -> wo3
func toNumeral(in []byte) []byte {
	for i, r := range string(in) {
		// There can only be one diacritic in a valid Pinyin "word",
		// so just append the tone and return
		if toneChar, found := diacriticToToneChar[r]; found {
			newRunes := []rune(string(in))
			// ASCII prefix turns 48 -> '0'
			newRunes = append(newRunes, rune(48+toneChar.Tone))
			newRunes[i] = toneChar.Char
			return []byte(string(newRunes))
		}
	}
	return in
}

// ConvertToNumerals converts Pinyin text marked with diacritics,
// to Pinyin marked by numerals.
// Ex. ConvertToNumerals("wǒ hěn hǎo") // wo3 hen3 hao3
func ConvertToNumerals(text string) string {
	replacedBytes := pinyinDiacriticWordRe.ReplaceAllFunc([]byte(text), toNumeral)
	return string(replacedBytes)
}
