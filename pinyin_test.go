package pinyin_test

import (
	"bitbucket.org/purohit/pinyin"
	"testing"
)

// TestPinyinConversion ensures conversions like wo3 hen3 hao3 <-> wǒ hěn hǎo.
func TestPinyinConversion(t *testing.T) {
	numeral := "wo3 hen3 hao3 shui3 zou4 ping2 zuo4 ba qiong2 er2 qie3 nv2 lv4"
	diacritic := "wǒ hěn hǎo shuǐ zòu píng zuò ba qióng ér qiě nǘ lǜ"
	convertedNumeral := pinyin.ConvertToNumerals(diacritic)
	convertedDiacritic := pinyin.ConvertToDiacritics(numeral)
	if diacritic != convertedDiacritic {
		t.Errorf("(expected) %s != %s (actual)", diacritic, convertedDiacritic)
	}
	if numeral != convertedNumeral {
		t.Errorf("(expected) %s != %s (actual)", numeral, convertedNumeral)
	}
}

// TestOneWayPinyin makes sure we can convert neutral tone integers, but not re-create them (yet).
func TestOneWayPinyin(t *testing.T) {
	numeral := "rou5 a5 wei3 xun5 shui5"
	expected := "rou a wěi xun shui"
	actual := pinyin.ConvertToDiacritics(numeral)
	if expected != actual {
		t.Errorf("(expected) %s != %s (actual)", expected, actual)
	}
}
