Package pinyin provides functions to convert text between Pinyin diacritic and
tone markings, e.g. wo3 <-> wǒ.

Read the docs on godoc.org: [http://godoc.org/bitbucket.org/purohit/pinyin](http://godoc.org/bitbucket.org/purohit/pinyin)
